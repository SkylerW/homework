//
//  Pulsar.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import UIKit

/// Simple pulse animation control object

final class Pulsar {
    
    private var pulseAnimation: CABasicAnimation?
    
    weak var view: UIView? {
        willSet {
            if let aView = view, (newValue == nil || newValue != aView) {
                aView.layer.removeAnimation(forKey: "pulsar")
                pulseAnimation = nil
            }
        }
        didSet {
            if let aView = view {
                pulseAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                pulseAnimation?.duration = 1.0
                pulseAnimation?.fromValue = 0.5
                pulseAnimation?.toValue = 1.0
                pulseAnimation?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                pulseAnimation?.autoreverses = true
                pulseAnimation?.repeatCount = .greatestFiniteMagnitude
                
                if let pulseAnimation = pulseAnimation {
                    aView.layer.add(pulseAnimation, forKey: "pulsar")
                }
            }
        }
    }
}
