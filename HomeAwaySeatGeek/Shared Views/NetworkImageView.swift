//
//  NetworkImageView.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import UIKit

/// Subclass of UIImageView handling loading from URL

final class NetworkImageView: UIImageView {
    
    private var currentURL: URL?
    
    func load(imageAtURL url: URL,
              usingImageService service: ImageService = ImageService.shared,
              imageMaxSize: Int? = nil,
              clearImage: Bool = true,
              useCache: Bool = true) {
        
        if let currentURL = currentURL, currentURL == url {
            return
        }
        
        if clearImage {
            image = nil
        }
        
        currentURL = url
        
        let size = imageMaxSize ?? Int(max(bounds.width, bounds.height) * UIScreen.main.scale)
                
        do {
            try service.fetchImage(withURL: url,
                                   imageMaxSize: size,
                                   useCache: useCache,
                                   response: {[weak self] result in
                
                switch result {
                case .failure(let error):
                    print(error)
                case .success(let pair):
                    guard let strongSelf = self, let currentURL = strongSelf.currentURL, currentURL == pair.url else {
                        return
                    }
                    
                    strongSelf.image = pair.image
                }
            })
            
        } catch let error {
            print(error)
        }
    }
}
