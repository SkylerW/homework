//
//  NetworkLayer.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/4/20.
//

import Foundation

protocol NetworkLayer {

    @discardableResult func dataRequest(reqest: URLRequest,
                                        dataTaskStrategy: DataTaskStrategy,
                                        autoStart: Bool) throws -> URLSessionDataTask?
}

final class NetworkLayerLive: NetworkLayer {
    
    static let shared: NetworkLayerLive = NetworkLayerLive()
    
    private let session: URLSession
        
    init(session: URLSession = .shared) {
        self.session = session
    }
        
    @discardableResult func dataRequest(reqest: URLRequest,
                                        dataTaskStrategy: DataTaskStrategy,
                                        autoStart: Bool = false) throws -> URLSessionDataTask? {

        guard reqest.url != nil else {
            return nil
        }
        
        let dataTask = session.dataTask(with: reqest, completionHandler: dataTaskStrategy.handler)
        
        if autoStart {
            dataTask.resume()
        }
        
        return dataTask
    }
}
