//
//  ParsingStrategies.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/4/20.
//

import Foundation
import UIKit
import ImageIO

// MARK: - DEFINITIONS
/// Various encapsulated strategies for parsing data from a network request
protocol DataTaskStrategy {
    var handler: ((Data?, URLResponse?, Error?) -> Void) { get }
}

enum DataTaskStrategyError: Error {
    case responseMissingData
}


// MARK: - STRATEGIES

/// Image parsing with sizing via the fast ImageIO thumbnailing API (for lack of a CDN with size requests)

struct ImageStrategy: DataTaskStrategy {
    
    enum ImageError: Error {
        case imageCreationError
    }
    
    let imageMaxSize: Int
    let completion: (Result<UIImage, Error>) -> Void
    
    var handler: (Data?, URLResponse?, Error?) -> Void {
        
        return { (data, res, error) in
            
            if let error = error {
                DispatchQueue.main.async {
                    self.completion(.failure(error))
                }
                return
            }
            
            if res == nil {
                print("missing response in ImageStrategy")
            }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    self.completion(.failure(DataTaskStrategyError.responseMissingData))
                }
                return
            }
            
            var finalImage = UIImage()
            
            let sizing = self.imageMaxSize
            let options = [kCGImageSourceCreateThumbnailFromImageAlways: true,
                           kCGImageSourceThumbnailMaxPixelSize: sizing] as CFDictionary
            
            if let source = CGImageSourceCreateWithData(data as CFData, nil),
                let imageReference = CGImageSourceCreateThumbnailAtIndex(source, 0, options) {
                
                finalImage = UIImage(cgImage: imageReference)
            
            } else if let image = UIImage(data: data) {
                finalImage = image
            }
            
            if finalImage.size == .zero {
                DispatchQueue.main.async {
                    self.completion(.failure(ImageError.imageCreationError))
                }
                return
            }
            
            DispatchQueue.main.async {
                self.completion(.success(finalImage))
            }
        }
    }
}


/// JSON Codable
struct JSONCodableStrategy<T: Codable>: DataTaskStrategy {
    
    enum JSONCodableStrategyError: Error {
        case parsingError
    }
    
    var decoder: JSONDecoder = {
        let d = JSONDecoder()
        d.keyDecodingStrategy = .convertFromSnakeCase
        return d
    }()
    
    let completion: (Result<T, Error>) -> Void
    
    var handler: (Data?, URLResponse?, Error?) -> Void {
        
        return { (data, res, error) in
            
            if let error = error {
                DispatchQueue.main.async {
                    self.completion(.failure(error))
                }
                return
            }
            
            if res == nil {
                print("missing response in JSONCodableStrategy")
            }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    self.completion(.failure(DataTaskStrategyError.responseMissingData))
                }
                return
            }
                        
            do {
                let responseObject = try decoder.decode(T.self, from: data)

                DispatchQueue.main.async {
                    self.completion(.success(responseObject))
                }
            } catch let error {
                
                DispatchQueue.main.async {
                    self.completion(.failure(error))
                }
            }
        }
    }
}
