//
//  Route.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation

// MARK: - VERB
enum HTTPVerb: CustomStringConvertible {
    case GET, POST
            
    var description: String {
        switch self {
        case .GET:
            return "GET"
        case .POST:
            return "POST"
        }
    }
}

// MARK: - SCHEME
enum HTTPScheme: CustomStringConvertible {
    case secure, insecure
            
    var description: String {
        switch self {
        case .insecure:
            return "http"
        case .secure:
            return "https"
        }
    }
}

// MARK: - HOST
enum Host: CustomStringConvertible {
    case seatGeekAPI
    
    var description: String {
        switch self{
        case .seatGeekAPI:
            return "api.seatgeek.com"
        }
    }
}

// MARK: - ROUTE
enum Route {
    
    case events(params: [URLQueryItem]?)
        
    var scheme: HTTPScheme {
        switch self {
        case .events:
            return .secure
        }
    }
    
    var host: Host {
        switch self {
        case .events:
            return .seatGeekAPI
        }
    }
        
    var path: String {
        switch self {
        case .events:
            return "/2/events"
        }
    }
    
    var components: URLComponents {
        var c = URLComponents()
        c.scheme = scheme.description
        c.host = host.description
        c.path = path
        
        switch self {
        case .events(params: let params):
            c.queryItems = params
        }
        
        return c
    }
}

extension Route {
    
    var eventSearchQueryRequest: URLRequest? {
        
        guard let url = components.url else {
            return nil
        }
                
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPVerb.GET.description
        
        return urlRequest
    }
}
