//
//  EventAPI.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation

struct EventQueryResponse: Codable {
    let events: [EventQueryEvent]
    let meta: EventQueryMeta
}

struct EventQueryMeta: Codable {
    let page: Int
    let total: Int
    let perPage: Int
}

struct EventQueryEvent: Codable {
    let type: String
    let id: Int
    let datetimeUtc: String
    let title: String
    let venue: EventQueryVenue
    let performers: [EventQueryPerformer]
}

struct EventQueryVenue: Codable {
    let city: String
    let state: String
}

struct EventQueryPerformer: Codable {
    var image: String?
}
