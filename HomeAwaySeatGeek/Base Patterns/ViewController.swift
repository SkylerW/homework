//
//  ViewController.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation
import UIKit
import Combine


// MARK: - BASE
class ViewController<VM: ViewModel, R: Router, I: Interactor<VM, R>>: UIViewController {
    
    let interactor: I
    let viewModel: VM
    
    var viewTitleSubscriber: AnyCancellable?
    
    init(interactor: I) {
        self.interactor = interactor
        self.viewModel = interactor.viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        viewTitleSubscriber = viewModel.viewTitle.sink(receiveValue: {[weak self] title in
            self?.title = title
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - COLLECTION UI
class CollectionUIViewController<VM: CollectionUIViewModel, R: Router, I: CollectionUIInteractor<VM, R>>: ViewController<VM, R, I> {
    
    lazy var collectionView: UICollectionView = {
        let v = UICollectionView(frame: .zero,
                                 collectionViewLayout: viewModel.collectionViewLayout.value)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.delegate = interactor.collectionViewManager
        v.dataSource = interactor.collectionViewManager
        return v
    }()
    
    var collectionViewInstructionSubscriber: AnyCancellable?
    var collectionViewBackgroundColorSubscriber: AnyCancellable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        
        collectionView.backgroundColor = viewModel.collectionViewBackgroundColor.value
        
        setupCollectionViewConstraints()
        setupCollectionViewInstructions()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        interactor.safeToLayoutCollectionView()
    }
    
    open func setupCollectionViewConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    open func setupCollectionViewInstructions() {
        
        collectionViewInstructionSubscriber = viewModel.collectionViewInstruction.sink(receiveValue: {[weak self] instruction in
            
            guard let collectionView = self?.collectionView else {
                return
            }
            
            switch instruction {
            case .reloadData:
                collectionView.reloadData()
            case .sectionReload(indexes: let set):
                collectionView.reloadSections(set)
            case .sectionInsert(indexes: let set):
                collectionView.insertSections(set)
            case .sectionRemoval(indexes: let set):
                collectionView.deleteSections(set)
            case .sectionMove(from: let from, to: let to):
                collectionView.moveSection(from, toSection: to)
            case .itemReload(indexPaths: let paths):
                collectionView.reloadItems(at: paths)
            case .itemInsert(indexPaths: let paths):
                collectionView.insertItems(at: paths)
            case .itemRemoval(indexPaths: let paths):
                collectionView.deleteItems(at: paths)
            case .itemMove(from: let from, to: let to):
                collectionView.moveItem(at: from, to: to)
            case .batchUpdate(let update, let completion):
                collectionView.performBatchUpdates(update, completion: completion)
            case .changeLayout(let layout, animated: let animated, completion: let completion):
                collectionView.setCollectionViewLayout(layout, animated: animated, completion: completion)
            case .registerCells(cellData: let cellData):
                cellData.forEach { collectionView.register($0.class, forCellWithReuseIdentifier: $0.reuseID) }
            case .registerSupplementaryViews(viewData: let viewData):
                viewData.forEach { collectionView.register($0.class, forSupplementaryViewOfKind: $0.type, withReuseIdentifier: $0.reuseID) }
            case .scrollToItem(at: let at, position: let position, animated: let animated):
                collectionView.scrollToItem(at: at, at: position, animated: animated)
            case .insetContent(inset: let inset):
                collectionView.contentInset = inset
            case .verticalScroller(show: let show):
                collectionView.showsVerticalScrollIndicator = show
            case .horizontalScroller(show: let show):
                collectionView.showsHorizontalScrollIndicator = show
            case .scrollEnabled(enabled: let enabled):
                collectionView.isScrollEnabled = enabled
            }
        })
    }
}
