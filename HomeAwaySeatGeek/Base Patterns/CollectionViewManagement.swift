//
//  CollectionViewManagement.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation
import UIKit

// MARK: - Configurators and data structures
protocol Delegatable {
    func assign(delegate: AnyObject)
}

protocol ViewConfigurator {
    func configure(view: UIView)
}

protocol CellConfigurator: ViewConfigurator {
    var cellIdentifier: String { get }
}

protocol CellHighlightingAndSelection {
    var shouldHighlight: Bool { get }
    var shouldSelect: Bool { get }
    var shouldDeselect: Bool { get }
}

protocol CellStaticHeight {
    var cellHeight: CGFloat { get set }
}

protocol CellStaticWidth {
    var cellWidth: CGFloat { get set }
}

protocol CollectionItem {
    var itemConfigurators: [CellConfigurator] { get set }
    var supplementaryConfigurators: [String: CellConfigurator] { get set }
}

protocol CollectionViewManageable {
    var sectionItem: CollectionItem { get set }
}

typealias CollectionViewCellRegistrationData = (class: AnyClass, reuseID: String)
typealias CollectionViewSupplementaryRegistrationData = (class: AnyClass, type: String, reuseID: String)

protocol CollectionViewActionable: class {
    var receiveCollectionViewInstruction: ((CollectionViewUpdateInstruction) -> Void)? { get set}
}

/// two structs that are basic conformers to the colletion protocols
struct BasicCollectionItem: CollectionItem {
    
    var itemConfigurators: [CellConfigurator]
    var supplementaryConfigurators: [String: CellConfigurator]
    
    init(items: [CellConfigurator], supplements: [String: CellConfigurator] = [:]) {
        itemConfigurators = items
        supplementaryConfigurators = supplements
    }
}

struct BasicManageableCollection: CollectionViewManageable {
    
    var sectionItem: CollectionItem
    
    init(collectionItem item: CollectionItem) {
        sectionItem = item
    }
}

extension UICollectionReusableView {
    
    static func cellID() -> String {
        return String(describing: self)
    }
    
    static func classRegistrationData() -> CollectionViewCellRegistrationData {
        return (self, self.cellID())
    }
}

enum CollectionViewUpdateInstruction {
    case reloadData,
    sectionReload(indexes: IndexSet),
    sectionInsert(indexes: IndexSet),
    sectionRemoval(indexes: IndexSet),
    sectionMove(from: Int, to: Int),
    itemReload(indexPaths: [IndexPath]),
    itemInsert(indexPaths: [IndexPath]),
    itemRemoval(indexPaths: [IndexPath]),
    itemMove(from: IndexPath, to: IndexPath),
    batchUpdate(update: () -> Void, completion: ((Bool) -> Void)?),
    changeLayout(layout: UICollectionViewLayout, animated: Bool, completion: ((Bool) -> Void)?),
    registerCells(cellData: [CollectionViewCellRegistrationData]),
    registerSupplementaryViews(viewData: [CollectionViewSupplementaryRegistrationData]),
    scrollToItem(at: IndexPath, position: UICollectionView.ScrollPosition, animated: Bool),
    insetContent(inset: UIEdgeInsets),
    verticalScroller(show: Bool),
    horizontalScroller(show: Bool),
    scrollEnabled(enabled: Bool)
}

enum CollectionViewManagerUpdateInstruction {
    case sectionReload(updates: [Int: CollectionViewManageable]),
    sectionInsert(section: CollectionViewManageable, at: Int),
    sectionAppend(section: CollectionViewManageable),
    sectionRemoval(at: Int),
    sectionMove(from: Int, to: Int),
    itemReload(updates: [IndexPath: CellConfigurator]),
    itemInsert(item: CellConfigurator, indexPath: IndexPath),
    itemAppend(item: CellConfigurator, section: Int),
    itemRemoval(indexPath: IndexPath),
    itemMove(from: IndexPath, to: IndexPath)
}


// MARK: - CollectionView Interface Segregations
protocol CollectionViewManagerDelegateSelection: class {

    func select(for configurator: CellConfigurator, indexPath: IndexPath)
    func deselect(for configurator: CellConfigurator, indexPath: IndexPath)
}

protocol CollectionViewManagerDelegateHighlighting: class {

    func highlight(for configurator: CellConfigurator, indexPath: IndexPath)
    func unHighlight(for configurator: CellConfigurator, indexPath: IndexPath)
}

protocol CollectionViewManagerDelegateDisplay: class {
    
    func displayedCell(for configurator: CellConfigurator, indexPath: IndexPath)
    func displayedSupplementaryView(for configurator: CellConfigurator, indexPath: IndexPath)
    func didEndDisplaying(for configurator: CellConfigurator, indexPath: IndexPath)
}

enum CollectionViewManagerDelegateTargetedPreviewState {
    case showing, dismissing
}

protocol CollectionViewManagerDelegatePreviewing: class {

    func previewConfiguration(for configurator: CellConfigurator, indexPath: IndexPath) -> UIContextMenuConfiguration?
    func targetedPreview(for configuration: UIContextMenuConfiguration,
                         with collectionView: UICollectionView,
                         state: CollectionViewManagerDelegateTargetedPreviewState) -> UITargetedPreview?
}

protocol CollectionViewManagerDelegatePrefetching: class {

    func prefetch(for configurators: [CellConfigurator])
    func cancelPrefetch(for configurators: [CellConfigurator])
}

// MARK: Manager
class CollectionViewManager: NSObject {

    lazy var manageableCollections: [CollectionViewManageable] = [CollectionViewManageable]()

    weak var delegate: AnyObject?
    weak var scrollViewDelegate: UIScrollViewDelegate?

    init(delegate: AnyObject?) {
        self.delegate = delegate
    }
    
    // Instructions
    func receiveInstruction(_ instruction: CollectionViewManagerUpdateInstruction) {
        switch instruction {
        case .sectionReload(updates: let updates):
            updates.forEach { manageableCollections[$0] = $1 }
        case .sectionInsert(section: let section, at: let index):
            manageableCollections.insert(section, at: index)
        case .sectionAppend(section: let section):
            manageableCollections.append(section)
        case .sectionRemoval(at: let index):
             manageableCollections.remove(at: index)
        case .sectionMove(from: let from, to: let to):
            let removed = manageableCollections.remove(at: from)
            manageableCollections.insert(removed, at: to)
        case .itemReload(updates: let updates):
            updates.forEach { manageableCollections[$0.section].sectionItem.itemConfigurators[$0.item] = $1 }
        case .itemInsert(item: let item, indexPath: let indexPath):
             manageableCollections[indexPath.section].sectionItem.itemConfigurators.insert(item, at: indexPath.item)
        case .itemAppend(item: let item, section: let section):
             manageableCollections[section].sectionItem.itemConfigurators.append(item)
        case .itemRemoval(indexPath: let indexPath):
            manageableCollections[indexPath.section].sectionItem.itemConfigurators.remove(at: indexPath.item)
        case .itemMove(from: let from, to: let to):
            let config = manageableCollections[from.section].sectionItem.itemConfigurators.remove(at: from.item)
            manageableCollections[to.section].sectionItem.itemConfigurators.insert(config, at: to.item)
        }
    }
        
    func numberOfItemsInSection(_ section: Int) -> Int {
        let collection = manageableCollections[section]
        return collection.sectionItem.itemConfigurators.count
    }
}

// MARK: UICollectionViewDataSource
extension CollectionViewManager: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return manageableCollections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return numberOfItemsInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collection = manageableCollections[indexPath.section]
        let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: configurator.cellIdentifier,
                                                      for: indexPath)
        
        if let delegate = delegate,
            let delegatable = cell as? Delegatable {
            delegatable.assign(delegate: delegate)
        }
        
        configurator.configure(view: cell)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        let collection = manageableCollections[indexPath.section]
        let sectionItem = collection.sectionItem
        let configurator = sectionItem.supplementaryConfigurators[kind]!
        
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                   withReuseIdentifier: configurator.cellIdentifier,
                                                                   for: indexPath)
        
        if let delegate = delegate,
            let delegatable = view as? Delegatable {
            delegatable.assign(delegate: delegate)
        }
        
        configurator.configure(view: view)
        
        return view
    }
}


// MARK: UICollectionViewDelegate
extension CollectionViewManager: UICollectionViewDelegate {
    
    
    // Highlighting
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        
        let collection = manageableCollections[indexPath.section]
        let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
        
        if let highlighter = configurator as? CellHighlightingAndSelection {
            return highlighter.shouldHighlight
        }
        
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {

        if let delegate = delegate as? CollectionViewManagerDelegateHighlighting {
            
            let collection = manageableCollections[indexPath.section]
            let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
            
            delegate.highlight(for: configurator, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        
        if let delegate = delegate as? CollectionViewManagerDelegateHighlighting {
            
            let collection = manageableCollections[indexPath.section]
            let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
            
            delegate.unHighlight(for: configurator, indexPath: indexPath)
        }
    }
    
    
    // Selection
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        let collection = manageableCollections[indexPath.section]
        let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
        
        if let selector = configurator as? CellHighlightingAndSelection {
            return selector.shouldSelect
        }
        
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let delegate = delegate as? CollectionViewManagerDelegateSelection {
           
            let collection = manageableCollections[indexPath.section]
            let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
            
            delegate.select(for: configurator, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        
        let collection = manageableCollections[indexPath.section]
        let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
        
        if let selector = configurator as? CellHighlightingAndSelection {
            return selector.shouldDeselect
        }
        
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let delegate = delegate as? CollectionViewManagerDelegateSelection {
            
            let collection = manageableCollections[indexPath.section]
            let configurator = collection.sectionItem.itemConfigurators[indexPath.item]

            delegate.deselect(for: configurator, indexPath: indexPath)
        }
    }
    
    
    // Display
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let collection = manageableCollections[indexPath.section]
        let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
                
        if let delegate = delegate as? CollectionViewManagerDelegateDisplay {
            delegate.displayedCell(for: configurator, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard indexPath.section < manageableCollections.count else {
            return
        }
        
        let collection = manageableCollections[indexPath.section]
       
        guard indexPath.item < collection.sectionItem.itemConfigurators.count else {
            return
        }
        
        let configurator = collection.sectionItem.itemConfigurators[indexPath.item]

        if let delegate = delegate as? CollectionViewManagerDelegateDisplay {
            delegate.didEndDisplaying(for: configurator, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, previewForHighlightingContextMenuWithConfiguration configuration: UIContextMenuConfiguration) -> UITargetedPreview? {
        
        if let delegate = delegate as? CollectionViewManagerDelegatePreviewing {
            return delegate.targetedPreview(for: configuration, with: collectionView, state: .showing)
        }
        
        return nil
    }
    
    func collectionView(_ collectionView: UICollectionView, previewForDismissingContextMenuWithConfiguration configuration: UIContextMenuConfiguration) -> UITargetedPreview? {
       
        if let delegate = delegate as? CollectionViewManagerDelegatePreviewing {
            return delegate.targetedPreview(for: configuration, with: collectionView, state: .dismissing)
        }
        
        return nil
    }

    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        if let delegate = delegate as? CollectionViewManagerDelegatePreviewing {
            
            let collection = manageableCollections[indexPath.section]
            let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
            
            return delegate.previewConfiguration(for: configurator, indexPath: indexPath)
        }
        
        return nil
    }
    
    // ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidScroll?(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollViewDelegate?.scrollViewDidEndDragging?(scrollView, willDecelerate: decelerate)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidEndScrollingAnimation?(scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidEndDecelerating?(scrollView)
    }
}

// MARK: - PREFETCHING
extension CollectionViewManager: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        if let delegate = delegate as? CollectionViewManagerDelegatePrefetching {
            
            var configs = [CellConfigurator]()
            
            for indexPath in indexPaths {
                let collection = manageableCollections[indexPath.section]
                let configurator = collection.sectionItem.itemConfigurators[indexPath.item]
                configs.append(configurator)
            }
            
             delegate.prefetch(for: configs)
        }
    }
}
