//
//  ViewModel.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation
import UIKit
import Combine


// MARK: - BASE
class ViewModel {
    
    var viewTitle = CurrentValueSubject<String, Never>("")
    var viewBackgroundColor = CurrentValueSubject<UIColor, Never>(.white)
}


// MARK: - COLLECTION UI
class CollectionUIViewModel: ViewModel {
    
    var collectionViewLayout = CurrentValueSubject<UICollectionViewLayout, Never>(UICollectionViewFlowLayout())
    var collectionViewInstruction = PassthroughSubject<CollectionViewUpdateInstruction, Never>()
    var collectionViewBackgroundColor = CurrentValueSubject<UIColor, Never>(UIColor(named: "collectionViewColor") ?? .white)
}
