//
//  Router.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation
import UIKit

protocol RouterDelegate: AnyObject {
    func shouldRoute(viewController: UIViewController, fromRouter: Router) -> Bool
    func willRoute(viewController: UIViewController, fromRouter: Router)
    func didRoute(viewController: UIViewController, fromRouter: Router)
}

// NOTE: You can of course go even futher with the router here if you need to
// by decoupling from UIViewController into some type of interface etc (so you can unit test state flow of presentation/push/etc more effectively)

class Router {
    weak var viewController: UIViewController?
    weak var delegate: RouterDelegate?
}
