//
//  Interactor.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation

// MARK: - BASE
class Interactor<VM: ViewModel, R: Router> {
    
    let viewModel: VM
    let router: R
    
    init(viewModel: VM, router: R) {
        self.viewModel = viewModel
        self.router = router
    }
    
    open func generateState() {}
}

// MARK: - COLLECTION UI
class CollectionUIInteractor<VM: ViewModel, R: Router>: Interactor<VM, R> {
    
    private(set) lazy var layoutReady = false
    
    lazy var collectionViewManager: CollectionViewManager = {
        let m = CollectionViewManager(delegate: self)
        return m
    }()
    
    open func safeToLayoutCollectionView() {
        if layoutReady == false {
            layoutReady = true
            generateState()
        }
    }
}
