//
//  FileIOLayer.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation

enum FileIODirectory {
    case privateDirectory
    
    var url: URL? {
        switch self {
        case .privateDirectory:
            return FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first
        }
    }
}

protocol FileIOLayer {
    func store(data: Data, to directory: URL, as fileName: String) throws
    func retrieve(dataNamed fileName: String, from directory: URL) -> Data?
}


// MARK: - DEVICE
struct FileIODevice: FileIOLayer {
    
    func store(data: Data, to directory: URL, as fileName: String) throws {
        
        if FileManager.default.fileExists(atPath: directory.path) == false {
            try FileManager.default.createDirectory(atPath: directory.path, withIntermediateDirectories: true, attributes: nil)
        }
        
        let writeURL = directory.appendingPathComponent(fileName, isDirectory: false)
        
        if FileManager.default.fileExists(atPath: writeURL.path) {
            try FileManager.default.removeItem(at: writeURL)
        }
        
        FileManager.default.createFile(atPath: writeURL.path, contents: data, attributes: nil)
    }
    
    
    func retrieve(dataNamed fileName: String, from directory: URL) -> Data? {
                
        let readURL = directory.appendingPathComponent(fileName, isDirectory: false)
        
        if let data = FileManager.default.contents(atPath: readURL.path) {
            return data
        
        } else {
            return nil
        }
    }
}
