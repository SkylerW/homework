//
//  OrderedSet.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation

final class OrderedSet<T: Hashable> {
    
    private lazy var objects: [T] = []
    private lazy var indexOfKey: [T: Int] = [:]
    
    var all: [T] {
        return objects
    }
    
    init() {}
    
    init(collection: [T]) {
        for hashable in collection {
            add(hashable)
        }
    }
    
    func add(_ object: T) {
       
        guard indexOfKey[object] == nil else {
            return
        }
        
        objects.append(object)
        indexOfKey[object] = objects.count - 1
    }
    
    func insert(_ object: T, at index: Int) {
        
        if objects.isEmpty == false && (index >= objects.count || index < 0) {
            assertionFailure("Index should be within bounds")
        }
        
        guard indexOfKey[object] == nil else {
            return
        }
        
        objects.insert(object, at: index)
        indexOfKey[object] = index
        
        for i in index..<objects.count {
            indexOfKey[objects[i]] = i
        }
    }
    
    func object(at index: Int) -> T {
        
        if index >= objects.count || index < 0 {
            assertionFailure("Index should be within bounds")
        }
        
        return objects[index]
    }
    
    func set(_ object: T, at index: Int) {
        
        if index >= objects.count || index < 0 {
            assertionFailure("Index should be within bounds")
        }
        
        guard indexOfKey[object] == nil else {
            return
        }
        
        indexOfKey.removeValue(forKey: objects[index])
        indexOfKey[object] = index
        objects[index] = object
    }
    
    @discardableResult func indexOf(_ object: T) -> Int? {
        return indexOfKey[object]
    }
    
    @discardableResult func remove(_ object: T) -> T? {
        
        guard let index = indexOfKey[object] else {
            return nil
        }
        
        indexOfKey.removeValue(forKey: object)
        
        let removedObject = objects.remove(at: index)
        
        for i in index..<objects.count {
            indexOfKey[objects[i]] = i
        }
        
        return removedObject
    }
    
    func removeAll() {
        objects.removeAll()
        indexOfKey.removeAll()
    }
}
