//
//  FavoritesService.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/4/20.
//

import Foundation
import Combine


/// An operational layer for performing the persistence and collection management of favorite events  encapsulating a file I/O layer

enum FavoriteUpdateAction {
    case added, removed
}

typealias FavoritesUpdate = (event: Event, action: FavoriteUpdateAction)

final class FavoritesService {

    static let shared: FavoritesService = FavoritesService()
    
    private var currentFavorites: OrderedSet<Event> = OrderedSet<Event>()
        
    private lazy var fileIOQueue: DispatchQueue = DispatchQueue(label: "ai.cyborg.fileIO.serial")
    
    private let fileIOLayer: FileIOLayer
    
    private let fileName: String
    
    let favoritesSubject = PassthroughSubject<FavoritesUpdate, Never>()
    
    
    init(fileIOLayer: FileIOLayer = FileIODevice(),
         fileName: String = "favorites.json") {
        
        self.fileIOLayer = fileIOLayer
        self.fileName = fileName
        
        loadFavorites()
    }
    
    private func loadFavorites() {
                
        fileIOQueue.async {
            
            guard let privateDirectoryURL = FileIODirectory.privateDirectory.url,
                  let data = self.fileIOLayer.retrieve(dataNamed: self.fileName,
                                                       from: privateDirectoryURL) else {
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let events = try decoder.decode([Event].self, from: data)
            
                events.forEach { e in
                    self.currentFavorites.add(e)
                }
                
            } catch let error {
                print(error)
            }
        }
    }
    
    func favorite(event: Event, notifyChange: Bool = true) {
        
        fileIOQueue.sync {
            
            if currentFavorites.indexOf(event) == nil {
                currentFavorites.insert(event, at: 0)
                
                if notifyChange {
                    DispatchQueue.main.async {
                        self.favoritesSubject.send((event, .added))
                    }
                }
                
                fileIOQueue.async {
                    
                    guard let privateDirectoryURL = FileIODirectory.privateDirectory.url else {
                        return
                    }
                    
                    do {
                        let encoder = JSONEncoder()
                        let data = try encoder.encode(self.currentFavorites.all)
                        try self.fileIOLayer.store(data: data, to: privateDirectoryURL, as: self.fileName)
                        
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func unfavorite(event: Event, notifyChange: Bool = true) {
        
        fileIOQueue.sync {
            
            if currentFavorites.indexOf(event) != nil {
                currentFavorites.remove(event)
                
                if notifyChange {
                    DispatchQueue.main.async {
                        self.favoritesSubject.send((event, .removed))
                    }
                }
                
                fileIOQueue.async {
                    
                    guard let privateDirectoryURL = FileIODirectory.privateDirectory.url else {
                        return
                    }
                    
                    do {
                        let encoder = JSONEncoder()
                        let data = try encoder.encode(self.currentFavorites.all)
                        try self.fileIOLayer.store(data: data, to: privateDirectoryURL, as: self.fileName)
                        
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func hasFavorite(event: Event) -> Bool {
        
        var fav = false
        
        fileIOQueue.sync {
            fav = currentFavorites.indexOf(event) != nil
        }
        
        return fav
    }
    
    func favorites() -> [Event] {
        
        var favs: [Event] = []
        
        fileIOQueue.sync {
            favs = currentFavorites.all
        }
        
        return favs
    }
    
    // super simple string search
    func favorites(fromSearchInput: SearchInput, response r: @escaping ([Event]) -> Void) {
        fileIOQueue.async {
            let filtered = self.currentFavorites.all.filter({
                return $0.keywords.contains(where: {
                    return $0.lowercased().contains(fromSearchInput.input.lowercased())
                })
            })
            DispatchQueue.main.async {
                r(filtered)
            }
        }
    }
}
