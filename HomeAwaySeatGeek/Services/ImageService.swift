//
//  ImageService.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/4/20.
//

import Foundation
import UIKit

/// An operational layer for fetching images encapsulating both a caching and a network layer

final class ImageService {
    
    typealias ImageURL = (image: UIImage, url: URL)
    typealias ImageServiceResponse = (Result<ImageURL, Error>) -> Void
    
    private typealias ImageOperation = (url: URL, response: ImageServiceResponse)
    
    static let shared: ImageService = ImageService()
    
    private let network: NetworkLayer
    
    private let cache: ImageCaching
    
    private lazy var activeOperations: [URL: [ImageServiceResponse]] = [:]
    private lazy var operationQueue: [ImageOperation] = []
    
    private let maxOperationCount: Int
    
    
    init(network: NetworkLayer = NetworkLayerLive.shared,
         cache: ImageCaching = ImageCache(),
         maxOperationCount: Int = 10) {
        
        self.network = network
        self.cache = cache
        self.maxOperationCount = maxOperationCount
    }
    
    
    // if we have a cached image, return it
    //
    // if we are at our max limit of operations we want to queue these responses up for later
    // otherwise lets look for an existing collection of responses or make a new collection
    //
    // Create a network image parsing strategy
    // with its completion we will operate on our cache, active operations and pending queue
    //
    // finally a network request
    
    func fetchImage(withURL url: URL,
                    imageMaxSize: Int,
                    useCache: Bool = true,
                    response r: @escaping ImageServiceResponse) throws {
        
        if useCache, let existingImage = cache.image(forURL: url) {
            r(.success((existingImage, url)))
            return
        }
        
        if activeOperations.count == maxOperationCount && activeOperations[url] == nil {
            operationQueue.insert((url, r), at: 0)
            return
        }
        
        if activeOperations[url] != nil  {
            activeOperations[url]?.append(r)
        } else {
            activeOperations[url] = [r]
        }
        
        let strategy = ImageStrategy(imageMaxSize: imageMaxSize, completion: { result in
            
            switch result {
            case .failure(let error):
                self.activeOperations[url]?.forEach { res in
                    res(.failure(error))
                }
            case .success(let image):
                if useCache {
                    self.cache.insert(image: image, forURL: url)
                }
                self.activeOperations[url]?.forEach { res in
                    res(.success((image, url)))
                }
            }
            
            self.activeOperations[url] = nil
            
            if let nextOp = self.operationQueue.popLast() {
                
                let newOps: [ImageServiceResponse] = [nextOp.response]
                let additionalMatches: [ImageServiceResponse] = self.operationQueue.filter { $0.url == nextOp.url }.map { $0.response }
                
                self.activeOperations[nextOp.url] = newOps + additionalMatches
                
                if additionalMatches.isEmpty == false {
                    self.operationQueue = self.operationQueue.filter { $0.url != nextOp.url }
                }
            }
        })
        
        try network.dataRequest(reqest: URLRequest(url: url), dataTaskStrategy: strategy, autoStart: true)
    }
}
