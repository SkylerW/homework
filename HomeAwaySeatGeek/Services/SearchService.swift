//
//  SearchService.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/4/20.
//

import Foundation

/// An operational layer for calling to the event search api encapsulating a network layer

struct SearchInput {
    
    let input: String
        
    init?(input: String) {
        
        // input validator etc can be here, simple for now, limit 50 chars
        guard input.isEmpty == false && input.count <= 50 else {
            return nil
        }
        self.input = input.lowercased()
    }
}

extension SearchInput: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(input)
    }
    
    static func == (lhs: SearchInput, rhs: SearchInput) -> Bool {
        return lhs.input == rhs.input
    }
}


// MARK: - Search Service
final class SearchService {
    
    // obfuscate secret to somewhere useful in production
    static let clientID: String = "MjEzNzg2OTV8MTYwNDU0ODMyMS45MzIwMzc4"
    
    typealias SearchServiceResponse = (Result<(meta: EventQueryMeta, events: [Event]), Error>) -> Void
    
    private typealias SearchOperation = (task: URLSessionDataTask, responses: [SearchServiceResponse])
    
    static let shared: SearchService = SearchService()
    
    private let network: NetworkLayer
        
    private var activeOperations: [SearchInput: SearchOperation] = [:]
    
    private let maxOperationCount: Int
    
    
    init(network: NetworkLayer = NetworkLayerLive.shared,
         maxOperationCount: Int = 1) {
        
        self.network = network
        self.maxOperationCount = maxOperationCount
    }
    
    
    // if we are have no search operations OR we have one going but this search is different
    // we want to spin up a new network task, cancel the one we had going if there is one (the search input was different)
    //
    // make the parsing strategy and in its completion we will cache the response,
    // send out any responses from a shared task and clear the search input operation entry
    //
    // if it was a repeat search input, then we will collect it, and let it "share" the network data task
    
    func search(withInput input: SearchInput, page: Int, response r: @escaping SearchServiceResponse) throws {
                
        if activeOperations.isEmpty || (activeOperations.isEmpty == false && activeOperations[input] == nil) {
            
            let params = [URLQueryItem(name: "client_id",
                                       value: SearchService.clientID),
                          URLQueryItem(name: "q",
                                       value: input.input),
                          URLQueryItem(name: "page", value: "\(page)")]
            
            let route = Route.events(params: params)
            
            let strategy = JSONCodableStrategy<EventQueryResponse>(completion: { result in
                
                switch result {
                case .failure(let error):
                    self.activeOperations[input]?.responses.forEach { res in
                        res(.failure(error))
                    }
                case .success(let eventQueryResponse):
                    
                    let events = eventQueryResponse.systemModels
                    
                    self.activeOperations[input]?.responses.forEach { res in
                        res(.success((eventQueryResponse.meta, events)))
                    }
                }
                
                self.activeOperations[input] = nil
            })
            
            guard let request = route.eventSearchQueryRequest,
                  let task = try network.dataRequest(reqest: request,
                                                     dataTaskStrategy: strategy,
                                                     autoStart: false) else {
                return
            }
            
            if cancelActiveSearchOperation(forInput: input) == false {
                activeOperations[input] = (task, [r])
            }
                        
            task.resume()
        
        } else if activeOperations[input] != nil {
            activeOperations[input]?.responses.append(r)
        }
    }
    
    @discardableResult func cancelActiveSearchOperation(forInput input: SearchInput) -> Bool {
        
        if activeOperations.isEmpty == false && activeOperations[input] == nil {
            activeOperations.first?.value.task.cancel()
            activeOperations.removeAll()
            return true
        }
        
        return false
    }
}
