//
//  Event.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation

struct Location: Codable, CustomStringConvertible {
    let city: String
    let state: String
    
    var description: String {
        return "\(city), \(state)"
    }
}

struct Event: Codable {
    let id: Int
    let title: String
    let location: Location
    let date: Date
    var imageURL: String?
    let keywords: [String]
}

extension Event: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Event, rhs: Event) -> Bool {
        return lhs.id == rhs.id
    }
}


extension EventQueryResponse {
    
    var systemModels: [Event] {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // 2020-12-08T04:00:00

        return events.compactMap({ event in
            
            var image: String?
            if let imageContaining = event.performers.first(where: { $0.image != nil }) {
                image = imageContaining.image
            }
            
            var keywords = Set<String>()
            keywords.insert(event.title.lowercased())
            keywords.insert(event.venue.city.lowercased())
            keywords.insert(event.venue.state.lowercased())
            
            return Event(id: event.id,
                         title: event.title,
                         location: Location(city: event.venue.city,
                                            state: event.venue.state),
                         date: dateFormatter.date(from: event.datetimeUtc) ?? Date(),
                         imageURL: image,
                         keywords: Array(keywords))
        })
    }
}
