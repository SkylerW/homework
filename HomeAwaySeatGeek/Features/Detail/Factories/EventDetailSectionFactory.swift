//
//  EventDetailSectionFactory.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/7/20.
//

import Foundation


struct EventDetailSectionFactory {
    
    func eventDetailSections(event: Event, favoriteService: FavoritesService) -> [CollectionViewManageable] {
        
        let configs: [CellConfigurator] = [
            EventDetailSingleTextCellConfigurator(text: event.title,
                                                  font: .systemFont(ofSize: 20.0,
                                                                    weight: .bold),
                                                  alignment: .left),
            
            EventDetailImageCellConfigurator(imageURL: event.imageURL,
                                             favorite: favoriteService.hasFavorite(event: event)),
            
            EventDetailSingleTextCellConfigurator(text: DateFormatter.eventDateFormatter.string(from: event.date),
                                                  font: .systemFont(ofSize: 15.0,
                                                                    weight: .medium),
                                                  alignment: .center),
            
            EventDetailSingleTextCellConfigurator(text: event.location.description,
                                                  font: .systemFont(ofSize: 13.0,
                                                                    weight: .regular),
                                                  alignment: .center)
        ]
        
        let sections: [CollectionViewManageable] = configs.compactMap { config in
            let item = BasicCollectionItem(items: [config])
            return BasicManageableCollection(collectionItem: item)
        }
        
        return sections
    }
}
