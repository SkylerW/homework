//
//  EventDetailInteractor.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation

final class EventDetailInteractor: CollectionUIInteractor<EventDetailViewModel, EventDetailRouter> {

    private let event: Event
    
    private let favoritesService: FavoritesService
    
    init(viewModel: EventDetailViewModel,
         router: EventDetailRouter,
         event: Event,
         favoritesService: FavoritesService = .shared) {
        self.event = event
        self.favoritesService = favoritesService
        super.init(viewModel: viewModel, router: router)
    }
    
    override func generateState() {
        
        viewModel.collectionViewInstruction.send(.verticalScroller(show: false))
        viewModel.collectionViewInstruction.send(.horizontalScroller(show: false))
        viewModel.collectionViewInstruction.send(.registerCells(cellData: [EventDetailImageCell.classRegistrationData(),
                                                                           EventDetailSingleTextCell.classRegistrationData()]))
        
        let sections = EventDetailSectionFactory().eventDetailSections(event: event, favoriteService: favoritesService)
        
        collectionViewManager.manageableCollections = sections
        viewModel.collectionViewInstruction.send(.reloadData)
    }
}
