//
//  EventDetailViewModel.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/7/20.
//

import Foundation
import UIKit
import Combine

final class EventDetailViewModel: CollectionUIViewModel {
    
    override init() {
        super.init()
        collectionViewLayout.send(UICollectionViewLayout.eventDetailLayout)
    }
}

extension UICollectionViewLayout {
    
    static var eventDetailLayout: UICollectionViewLayout {
        
        let layout = UICollectionViewCompositionalLayout(sectionProvider: { (sectionIndex, environment) -> NSCollectionLayoutSection? in

            switch sectionIndex {
            case 0, 2, 3:
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                             heightDimension: .estimated(50))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: .fixed(8), trailing: nil, bottom: .fixed(8))
                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                      heightDimension: .estimated(50))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                                 subitems: [item])

                return NSCollectionLayoutSection(group: group)

            case 1:
                
                let estimated = (UIScreen.main.bounds.width - 30.0) * 0.6

                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                             heightDimension: .estimated(estimated))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: .fixed(8), trailing: nil, bottom: .fixed(8))
                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                      heightDimension: .estimated(estimated))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                                 subitems: [item])

                return NSCollectionLayoutSection(group: group)

            default:
                return nil
            }
        })

        return layout
    }
}



