//
//  EventDetailBuilder.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/7/20.
//

import Foundation

final class EventDetailBuilder: Builder<EventDetailViewController> {
    
    private let event: Event
    
    init(event: Event) {
        self.event = event
        super.init()
    }
    
    override func build() -> EventDetailViewController? {
                
        let router = EventDetailRouter()
        let viewModel = EventDetailViewModel()
        let interactor = EventDetailInteractor(viewModel: viewModel,
                                               router: router,
                                               event: event)
        let viewController = EventDetailViewController(interactor: interactor)
        
        return viewController
    }
}
