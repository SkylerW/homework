//
//  EventDetailSingleTextCell.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/7/20.
//

import Foundation
import UIKit

struct EventDetailSingleTextCellConfigurator: CellConfigurator {
        
    let cellIdentifier: String = EventDetailSingleTextCell.cellID()
    
    let text: String
    let font: UIFont
    let alignment: NSTextAlignment
    
    func configure(view: UIView) {
        guard let cell = view as? EventDetailSingleTextCell else {
            return
        }
        
        cell.label.font = font
        cell.label.text = text
        cell.label.textAlignment = alignment
    }
}

final class EventDetailSingleTextCell: UICollectionViewCell {
        
    private(set) lazy var label: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor(named: "fontColor")
        v.numberOfLines = 0
        v.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 30.0
        return v
    }()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        
        contentView.addSubview(label)
                
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15.0),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15.0),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
