//
//  EventDetailImageCell.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/7/20.
//

import Foundation
import UIKit

struct EventDetailImageCellConfigurator: CellConfigurator {
        
    let cellIdentifier: String = EventDetailImageCell.cellID()
    
    let imageURL: String?
    let favorite: Bool
    
    func configure(view: UIView) {
        guard let cell = view as? EventDetailImageCell else {
            return
        }
        
        if let urlString = imageURL, let url = URL(string: urlString) {
            cell.eventImage.load(imageAtURL: url,
                                 imageMaxSize: EventDetailImageCell.optimizedImageSize,
                                 useCache: false)
        }
        
        cell.favoriteImage.isHidden = favorite == false
    }
}

final class EventDetailImageCell: UICollectionViewCell {
    
    static var optimizedImageSize: Int = Int(UIScreen.main.bounds.width - 30.0)
            
    private(set) lazy var eventImage: NetworkImageView = {
        let v = NetworkImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.clipsToBounds = true
        v.contentMode = .scaleAspectFill
        v.backgroundColor = .lightGray
        v.layer.cornerRadius = 5.0
        return v
    }()
    
    private(set) lazy var favoriteImage: UIImageView = {
        let v = UIImageView(image: UIImage(systemName: "heart.fill")?.withRenderingMode(.alwaysTemplate))
        v.tintColor = .red
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentMode = .scaleAspectFit
        return v
    }()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
                
        contentView.addSubview(eventImage)
        contentView.addSubview(favoriteImage)

        NSLayoutConstraint.activate([
            eventImage.topAnchor.constraint(equalTo: contentView.topAnchor),
            eventImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            eventImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15.0),
            eventImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15.0),
            
            favoriteImage.widthAnchor.constraint(equalToConstant: 20.0),
            favoriteImage.heightAnchor.constraint(equalToConstant: 20.0),
            favoriteImage.centerYAnchor.constraint(equalTo: eventImage.topAnchor),
            favoriteImage.centerXAnchor.constraint(equalTo: eventImage.leadingAnchor)
        ])
    }
}
