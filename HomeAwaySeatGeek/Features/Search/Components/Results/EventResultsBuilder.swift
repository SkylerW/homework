//
//  EventResultsBuilder.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation

final class EventResultsBuilder: Builder<EventResultsViewController> {
    
    override func build() -> EventResultsViewController? {
                
        let router = EventResultsRouter()
        let viewModel = EventResultsViewModel()
        let interactor = EventResultsInteractor(viewModel: viewModel, router: router)
        let viewController = EventResultsViewController(interactor: interactor)
        
        return viewController
    }
}
