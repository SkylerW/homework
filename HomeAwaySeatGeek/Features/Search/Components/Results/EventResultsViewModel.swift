//
//  EventResultsViewModel.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import UIKit
import Combine

final class EventResultsViewModel: CollectionUIViewModel {
    
    var dataLoading = CurrentValueSubject<Bool, Never>(false)
    
    override init() {
        super.init()
        collectionViewLayout.send(UICollectionViewLayout.eventSearchLayout)
    }
}

extension UICollectionViewLayout {
    
    static var eventSearchLayout: UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .absolute(150.0))
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
}
