//
//  EventResultsRouter.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation

final class EventResultsRouter: Router {
    
    func showEventDetail(event: Event) {
        
        let builder = EventDetailBuilder(event: event)
        guard let vc = builder.build() else {
            return
        }
        
        delegate?.willRoute(viewController: vc, fromRouter: self)
        
        if let delegate = delegate,
           delegate.shouldRoute(viewController: vc, fromRouter: self) == false {
            return
        }
        
        vc.modalPresentationStyle = .pageSheet
        viewController?.present(vc, animated: true, completion: nil)
        
        delegate?.didRoute(viewController: vc, fromRouter: self)
    }
}
