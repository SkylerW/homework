//
//  EventResultsInteractor.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import UIKit
import Combine

// MARK: - INTERACTOR
final class EventResultsInteractor: CollectionUIInteractor<EventResultsViewModel, EventResultsRouter> {
    
    private let favoritesService: FavoritesService
    private let imageService: ImageService
    
    private lazy var isLoading: Bool = false
    
    // we will use this so we don't have to search the collection multiple times during a preview
    private var cachedTargetedPreview: UITargetedPreview?
    
    init(viewModel: EventResultsViewModel,
         router: EventResultsRouter,
         favoritesService: FavoritesService = .shared,
         imageService: ImageService = .shared) {
        
        self.favoritesService = favoritesService
        self.imageService = imageService
        
        super.init(viewModel: viewModel, router: router)
    }
    
    override func generateState() {
        
        viewModel.collectionViewInstruction.send(.verticalScroller(show: false))
        viewModel.collectionViewInstruction.send(.horizontalScroller(show: false))
        viewModel.collectionViewInstruction.send(.registerCells(cellData: [SearchEventCell.classRegistrationData(),
                                                                           SearchEventSkeletonCell.classRegistrationData()]))
    }
    
    func displayLoadingState() {
        collectionViewManager.manageableCollections.removeAll()
        viewModel.collectionViewInstruction.send(.reloadData)
        
        var sections: [CollectionViewManageable] = []
        
        for _ in 0..<20 {
            let item = BasicCollectionItem(items: [SearchEventSkeletonCellConfigurator()])
            let section = BasicManageableCollection(collectionItem: item)
            sections.append(section)
        }
        
        collectionViewManager.manageableCollections = sections
        viewModel.collectionViewInstruction.send(.reloadData)
        viewModel.collectionViewInstruction.send(.scrollEnabled(enabled: false))
        viewModel.dataLoading.send(true)
        
        isLoading = true
    }
        
    func display(results: [Event], clearExisting: Bool = false) {
        
        let shouldClear = clearExisting || isLoading
        
        isLoading = false
        
        viewModel.dataLoading.send(false)
        
        let factory = EventResultsSectionFactory()
        let newSections = factory.eventResultsSections(events: results,
                                                       favoriteService: favoritesService)

        let initialSectionCount = self.collectionViewManager.manageableCollections.count
        
        if shouldClear {
            self.collectionViewManager.manageableCollections.removeAll()
        }
        
        let sectionCount = clearExisting ? 0 : initialSectionCount
        let newCount = sectionCount + newSections.count
        
        self.collectionViewManager.manageableCollections += newSections
        
        viewModel.collectionViewInstruction.send(.batchUpdate(update: {
            
            if shouldClear {
                self.viewModel.collectionViewInstruction.send(.sectionRemoval(indexes: IndexSet(integersIn: 0..<initialSectionCount)))
            }
    
            self.viewModel.collectionViewInstruction.send(.sectionInsert(indexes: IndexSet(integersIn: sectionCount..<newCount)))
            
        }, completion: { fin in
            self.viewModel.collectionViewInstruction.send(.scrollEnabled(enabled: true))
        }))
    }
    
    func reload(event: Event) {
        
        guard let index = collectionViewManager.manageableCollections.firstIndex(where: { section in
            guard let section = section as? EventSection else {
                return false
            }
            return section.event == event
        }) else {
            return
        }
        
        let newSection = EventResultsSectionFactory().eventResultsSections(events: [event],
                                                                           favoriteService: favoritesService)
        let updates: [Int: CollectionViewManageable] = [index: newSection[0]]
        collectionViewManager.receiveInstruction(.sectionReload(updates: updates))
        viewModel.collectionViewInstruction.send(.sectionReload(indexes: IndexSet(integer: index)))
    }
}

// MARK: - SELECTION
extension EventResultsInteractor: CollectionViewManagerDelegateSelection {
    
    func select(for configurator: CellConfigurator, indexPath: IndexPath) {
        guard let configurator = configurator as? SearchEventCellConfigurator else {
            return
        }
        router.showEventDetail(event: configurator.event)
    }
    
    func deselect(for configurator: CellConfigurator, indexPath: IndexPath) {}
}


// MARK: - PREFETCHING
extension EventResultsInteractor: CollectionViewManagerDelegatePrefetching {
    
    func prefetch(for configurators: [CellConfigurator]) {
        
        configurators.forEach { c in
            guard let config = c as? SearchEventCellConfigurator else {
                return
            }
            
            guard let urlString = config.event.imageURL,
                  let url = URL(string: urlString) else {
                return
            }
            
            do {
                try imageService.fetchImage(withURL: url,
                                            imageMaxSize: SearchEventCell.optimizedImageSize,
                                            response: { _ in })
            
            } catch let error {
                print(error)
            }
        }
    }
    
    func cancelPrefetch(for configurators: [CellConfigurator]) {}
}


// MARK: - PREVIEWING / FAVORITE
extension EventResultsInteractor: CollectionViewManagerDelegatePreviewing {
    
    func previewConfiguration(for configurator: CellConfigurator, indexPath: IndexPath) -> UIContextMenuConfiguration? {
        
        guard let configurator = configurator as? SearchEventCellConfigurator else {
            return nil
        }
        
        let identifier = NSString(string: "\(configurator.event.id)")
        
        let isFavorite = favoritesService.hasFavorite(event: configurator.event)
                                
        return UIContextMenuConfiguration(identifier: identifier, previewProvider: nil) { suggestedActions in
            
            let action: UIAction
            
            if isFavorite {
                action = UIAction(title: "Unfavorite",
                                  image: UIImage(systemName: "heart.slash"),
                                  attributes: .destructive) { action in
                    self.favoritesService.unfavorite(event: configurator.event)
                }
                
            } else {
                action = UIAction(title: "Favorite",
                                  image: UIImage(systemName: "heart")) { action in
                    self.favoritesService.favorite(event: configurator.event)
                }
            }
                                
            let favoriteMenu = UIMenu(title: "Actions", children: [action])
            
            return favoriteMenu
        }
    }
    
    func targetedPreview(for configuration: UIContextMenuConfiguration,
                         with collectionView: UICollectionView,
                         state: CollectionViewManagerDelegateTargetedPreviewState) -> UITargetedPreview? {
        
        if case state = CollectionViewManagerDelegateTargetedPreviewState.dismissing {
            let preview = cachedTargetedPreview
            self.cachedTargetedPreview = nil
            return preview
        }
        
        guard let identifier = configuration.identifier as? String else {
            return nil
        }
        
        guard let index = collectionViewManager.manageableCollections.firstIndex(where: { section in
            guard let section = section as? EventSection else {
                return false
            }
            return "\(section.event.id)" == identifier
        
        }) else {
            return nil
        }
        
        guard let cell = collectionView.cellForItem(at: IndexPath(item: 0, section: index)) as? SearchEventCell else {
            return nil
        }
    
        let parameters = UIPreviewParameters()
        parameters.backgroundColor = .clear

        let preview = UITargetedPreview(view: cell.eventImage, parameters: parameters)
        cachedTargetedPreview = preview
        
        return preview
    }
}
