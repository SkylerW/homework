//
//  EventResultsViewController.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import Combine
import UIKit

final class EventResultsViewController: CollectionUIViewController<EventResultsViewModel, EventResultsRouter, EventResultsInteractor> {
    
    private var dataLoadingSubscriber: AnyCancellable?
    
    private lazy var pulsar: Pulsar = Pulsar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataLoadingSubscriber = viewModel.dataLoading.sink(receiveValue: { [weak self] loading in
            self?.pulsar.view = loading ? self?.collectionView : nil
        })
    }
}
