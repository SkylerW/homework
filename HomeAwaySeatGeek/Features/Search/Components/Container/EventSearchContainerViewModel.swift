//
//  EventSearchContainerViewModel.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import Combine

enum EventSearchContainerViewState {
    case network, favorites
    
    init?(segmentedIndex: Int) {
        switch segmentedIndex {
        case 0:
            self = .network
        case 1:
            self = .favorites
        default:
            return nil
        }
    }
}

final class EventSearchContainerViewModel: ViewModel {
    var currentViewState = CurrentValueSubject<EventSearchContainerViewState, Never>(.network)
    var searchText = PassthroughSubject<String?, Never>()
}
