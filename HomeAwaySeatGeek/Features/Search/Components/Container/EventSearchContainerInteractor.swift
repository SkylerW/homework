//
//  EventSearchContainerInteractor.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import UIKit
import Combine

final class EventSearchContainerInteractor: Interactor<EventSearchContainerViewModel, EventSearchContainerRouter> {
    
    private let searchService: SearchService
    private let favoritesService: FavoritesService
    
    private unowned var networkResultsInteractor: EventResultsInteractor
    private unowned var favoritesResultsInteractor: EventResultsInteractor
    
    private var userFacingNetworkSearch: SearchInput?
    private var userFacingFavoritesSearch: SearchInput?
    
    private var paging: Bool = false
    private var networkSearchAndMeta: (SearchInput, EventQueryMeta)?
    
    private var currentState: EventSearchContainerViewState {
        return viewModel.currentViewState.value
    }
    
    private var favoritesSubscriber: AnyCancellable?
    
    init(viewModel: EventSearchContainerViewModel,
         router: EventSearchContainerRouter,
         searchService: SearchService = .shared,
         favoritesService: FavoritesService = .shared,
         networkResultsInteractor: EventResultsInteractor,
         favoritesResultsInteractor: EventResultsInteractor) {
        
        self.searchService = searchService
        self.favoritesService = favoritesService
        self.networkResultsInteractor = networkResultsInteractor
        self.favoritesResultsInteractor = favoritesResultsInteractor
        
        super.init(viewModel: viewModel, router: router)
    }
    
    override func generateState() {
        
        viewModel.viewTitle.send("Events")
        
        favoritesSubscriber = favoritesService.favoritesSubject.sink(receiveValue: {[weak self] update in
            
            self?.favoritesResultsInteractor.display(results: self?.favoritesService.favorites() ?? [],
                                                     clearExisting: true)
            
            self?.networkResultsInteractor.reload(event: update.event)
        })
        
        networkResultsInteractor.displayLoadingState()
        
        // TODO: pre-populate from network should be based on user location or some criteria etc...
        initiateNetworkSearch(input: "coachella", page: 1)
        
        favoritesResultsInteractor.display(results: favoritesService.favorites(),
                                           clearExisting: true)
    }
        
    private func initiateNetworkSearch(input: String, page: Int) {
        
        guard let input = SearchInput(input: input) else {
            if let existing = networkSearchAndMeta {
                searchService.cancelActiveSearchOperation(forInput: existing.0)
            }
            networkResultsInteractor.display(results: [], clearExisting: true)
            return
        }
        
        do {
            try searchService.search(withInput: input, page: page, response: { result in
                
                switch result {
                case .success(let searchRes):
                    self.networkSearchAndMeta = (input, searchRes.meta)
                    self.networkResultsInteractor.display(results: searchRes.events, clearExisting: searchRes.meta.page == 1)
                case .failure(let error):
                    print(String(describing: error))
                }
            })
        } catch let error {
            print(String(describing: error))
        }
    }
    
    private func initiateFavoritesSearch(input: String) {
        
        guard let input = SearchInput(input: input) else {
            favoritesResultsInteractor.display(results: favoritesService.favorites(),
                                               clearExisting: true)
            return
        }
        
        favoritesService.favorites(fromSearchInput: input, response: { events in
            self.favoritesResultsInteractor.display(results: events, clearExisting: true)
        })
    }
    
    func segmentedControlPressed(index: Int) {
        guard let newState = EventSearchContainerViewState(segmentedIndex: index),
              newState != currentState else {
            return
        }
        
        viewModel.currentViewState.send(newState)
        switch newState {
        case .network:
            viewModel.searchText.send(userFacingNetworkSearch?.input)
        case .favorites:
            viewModel.searchText.send(userFacingFavoritesSearch?.input)
        }
    }
    
    func searchShouldInitiate(input: String) {
                
        switch currentState {
        case .network:
            userFacingNetworkSearch = SearchInput(input: input)
            initiateNetworkSearch(input: input, page: 1)
            paging = false
        case .favorites:
            userFacingFavoritesSearch = SearchInput(input: input)
            initiateFavoritesSearch(input: input)
        }
    }
    
    func searchShouldPage() {
        switch currentState {
        case .network:
            guard paging == false else {
                return
            }
            if let tuple = networkSearchAndMeta, (tuple.1.page * tuple.1.perPage) < tuple.1.total {
                paging = true
                initiateNetworkSearch(input: tuple.0.input, page: tuple.1.page + 1)
            }
        case .favorites:
            break
        }
    }
        
    func shouldClearPersistedSearchTextIfNeeded() {
        
        switch currentState {
        case .network:
            userFacingNetworkSearch = nil
        case .favorites:
            userFacingFavoritesSearch = nil
        }
    }
}
