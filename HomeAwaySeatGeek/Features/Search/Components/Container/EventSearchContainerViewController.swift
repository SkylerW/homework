//
//  EventSearchContainerViewController.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation
import UIKit
import Combine

final class EventSearchContainerViewController: ViewController<EventSearchContainerViewModel, EventSearchContainerRouter, EventSearchContainerInteractor> {
        
    private lazy var segmentedControl: UISegmentedControl = {
        let v = UISegmentedControl(items: [NSLocalizedString("Discover", comment: ""),
                                           NSLocalizedString("Favorites", comment: "")])
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        v.selectedSegmentTintColor = .white
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "AppleSDGothicNeo-Light", size: 18.0)!,
                                                    .foregroundColor: UIColor(named: "fontColor") ?? .black,
                                                    .kern: 1.0,
                                                    .paragraphStyle: paragraphStyle]
        v.setTitleTextAttributes(attrs, for: .normal)
        v.setTitleTextAttributes(attrs, for: .selected)
        self.view.addSubview(v)
        v.addTarget(self, action: #selector(segmentChanged), for: .valueChanged)
        return v
    }()
    
    private lazy var blurView: UIVisualEffectView = {
        let v = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
       v.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(v)
       return v
    }()
    
    private lazy var searchBar: UISearchBar = {
        let v = UISearchBar()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.delegate = self
        v.backgroundColor = .clear
        v.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        v.tintColor = .white
        v.isTranslucent = true
        v.searchTextField.backgroundColor = .white
        v.showsCancelButton = true
        self.view.addSubview(v)
        return v
    }()
    
    let networkResultsViewController: EventResultsViewController
    let favoritesResultsViewController: EventResultsViewController
    
    private var viewStateSubscriber: AnyCancellable?
    private var searchTextSubscriber: AnyCancellable?
    
    private var networkResultsLeadingConstraint: NSLayoutConstraint?
    
    init(interactor: EventSearchContainerInteractor,
         networkResultsViewController: EventResultsViewController,
         favoritesResultsViewController: EventResultsViewController) {
        self.networkResultsViewController = networkResultsViewController
        self.favoritesResultsViewController = favoritesResultsViewController
        super.init(interactor: interactor)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupSubscribers()
        interactor.generateState()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if searchBar.isFirstResponder {
            searchBar.resignFirstResponder()
        }
    }
    
    private func setupView() {
        
        view.backgroundColor = UIColor(named: "collectionViewColor")
        
        addChild(networkResultsViewController)
        addChild(favoritesResultsViewController)
        
        networkResultsViewController.didMove(toParent: self)
        favoritesResultsViewController.didMove(toParent: self)
        
        networkResultsViewController.view.translatesAutoresizingMaskIntoConstraints = false
        favoritesResultsViewController.view.translatesAutoresizingMaskIntoConstraints = false
                
        view.addSubview(segmentedControl)
        view.addSubview(searchBar)
        view.addSubview(networkResultsViewController.view)
        view.addSubview(favoritesResultsViewController.view)
        
        networkResultsLeadingConstraint = networkResultsViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        networkResultsLeadingConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
           
            segmentedControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15.0),
            segmentedControl.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15.0),
            segmentedControl.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15.0),
            
            searchBar.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 10.0),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10.0),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10.0),
            
            networkResultsViewController.view.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 10.0),
            networkResultsViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            networkResultsViewController.view.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            favoritesResultsViewController.view.topAnchor.constraint(equalTo: networkResultsViewController.view.topAnchor),
            favoritesResultsViewController.view.bottomAnchor.constraint(equalTo: networkResultsViewController.view.bottomAnchor),
            favoritesResultsViewController.view.leadingAnchor.constraint(equalTo: networkResultsViewController.view.trailingAnchor),
            favoritesResultsViewController.view.widthAnchor.constraint(equalTo: networkResultsViewController.view.widthAnchor),
        ])
                
        switch viewModel.currentViewState.value {
        case .network:
            segmentedControl.selectedSegmentIndex = 0
        case .favorites:
            segmentedControl.selectedSegmentIndex = 1
        }
        
        view.setNeedsUpdateConstraints()
        view.updateConstraints()
        view.layoutIfNeeded()
    }
    
    private func setupSubscribers() {
        
        viewStateSubscriber = viewModel.currentViewState.sink(receiveValue: {[weak self] state in
            
            guard let strongSelf = self else {
                return
            }
            
            if strongSelf.searchBar.isFirstResponder {
                strongSelf.searchBar.resignFirstResponder()
            }
            
            switch state {
            case .network:
                strongSelf.favoritesResultsViewController.beginAppearanceTransition(false, animated: false)
                strongSelf.networkResultsViewController.beginAppearanceTransition(true, animated: true)
                
                strongSelf.networkResultsLeadingConstraint?.constant = 0.0
                
                UIView.animate(withDuration: 0.3,
                               delay: 0.0,
                               options: .curveEaseInOut,
                               animations: {
                                self?.view.layoutIfNeeded()
                                
                }, completion: { fin in
                    strongSelf.favoritesResultsViewController.endAppearanceTransition()
                    strongSelf.networkResultsViewController.endAppearanceTransition()
                })
                
            case .favorites:
                strongSelf.networkResultsViewController.beginAppearanceTransition(false, animated: false)
                strongSelf.favoritesResultsViewController.beginAppearanceTransition(true, animated: true)
                
                strongSelf.networkResultsLeadingConstraint?.constant = -UIScreen.main.bounds.width
                
                UIView.animate(withDuration: 0.3,
                               delay: 0.0,
                               options: .curveEaseInOut,
                               animations: {
                                self?.view.layoutIfNeeded()
                                
                }, completion: { fin in
                    strongSelf.networkResultsViewController.endAppearanceTransition()
                    strongSelf.favoritesResultsViewController.endAppearanceTransition()
                })
            }
        })
        
        searchTextSubscriber = viewModel.searchText.sink(receiveValue: { [weak self] text in
            self?.searchBar.text = text
        })
    }
    
    @objc private func segmentChanged() {
        interactor.segmentedControlPressed(index: segmentedControl.selectedSegmentIndex)
    }
}


// MARK: - SEARCH BAR
extension EventSearchContainerViewController: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let text = searchBar.text else {
            interactor.shouldClearPersistedSearchTextIfNeeded()
            return
        }
        
        interactor.searchShouldInitiate(input: text)
    }

    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let str = searchBar.text ?? "" + text
        
        if str.count <= 50 {
            return true
        }
        
        searchBar.text = String(str[str.startIndex...str.index(str.startIndex, offsetBy: 50)])
        
        return false
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension EventSearchContainerViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
            interactor.searchShouldPage()
        }
    }
}
