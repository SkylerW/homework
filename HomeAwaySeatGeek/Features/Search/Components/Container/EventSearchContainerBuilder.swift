//
//  EventSearchContainerBuilder.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation

final class EventSearchContainerBuilder: Builder<EventSearchContainerViewController> {
    
    override func build() -> EventSearchContainerViewController? {
        
        let resultsBuilder = EventResultsBuilder()
        
        guard let networkVC = resultsBuilder.build(),
              let favoritesVC = resultsBuilder.build() else {
            return nil
        }
        
        let router = EventSearchContainerRouter()
        let viewModel = EventSearchContainerViewModel()
        let interactor = EventSearchContainerInteractor(viewModel: viewModel,
                                                        router: router,
                                                        networkResultsInteractor: networkVC.interactor,
                                                        favoritesResultsInteractor: favoritesVC.interactor)
        let viewController = EventSearchContainerViewController(interactor: interactor,
                                                                networkResultsViewController: networkVC,
                                                                favoritesResultsViewController: favoritesVC)
        networkVC.interactor.router.viewController = viewController
        networkVC.interactor.collectionViewManager.scrollViewDelegate = viewController
        favoritesVC.interactor.router.viewController = viewController
        
        return viewController
    }
}
