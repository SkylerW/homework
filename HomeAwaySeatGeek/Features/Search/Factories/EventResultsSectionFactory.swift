//
//  EventResultsSectionFactory.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/6/20.
//

import Foundation

extension DateFormatter {
    
    static let eventDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm E, d MMM y"
        return formatter
    }()
}

struct EventSection: CollectionViewManageable {
    
    var sectionItem: CollectionItem
    let event: Event
}

struct EventResultsSectionFactory {
    
    func eventResultsSections(events: [Event], favoriteService: FavoritesService) -> [CollectionViewManageable] {
        
        let sections: [CollectionViewManageable] = events.compactMap({ event in
            
            let config = SearchEventCellConfigurator(event: event,
                                                     favorite: favoriteService.hasFavorite(event: event))
            
            let item = BasicCollectionItem(items: [config])
            
            return EventSection(sectionItem: item, event: event)
        })
        
        return sections
    }
}
