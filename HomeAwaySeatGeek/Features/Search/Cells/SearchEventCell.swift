//
//  SearchEventCell.swift
//  HomeAwaySeatGeek
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation
import UIKit


// MARK: - REAL
struct SearchEventCellConfigurator: CellConfigurator, CellHighlightingAndSelection {
        
    let cellIdentifier: String = SearchEventCell.cellID()
    
    let shouldHighlight: Bool = true
    let shouldSelect: Bool = true
    let shouldDeselect: Bool = false
    
    let event: Event
    let favorite: Bool
    
    func configure(view: UIView) {
        guard let cell = view as? SearchEventCell else {
            return
        }
        
        cell.titleLabel.text = event.title
        cell.locationLabel.text = event.location.description
        cell.dateLabel.text = DateFormatter.eventDateFormatter.string(from: event.date)
        
        if let urlString = event.imageURL, let url = URL(string: urlString) {
            cell.eventImage.load(imageAtURL: url,
                                 imageMaxSize: SearchEventCell.optimizedImageSize * 2)
        }
        
        cell.favoriteImage.isHidden = favorite == false
    }
}

final class SearchEventCell: UICollectionViewCell {
    
    static var optimizedImageSize: Int = 50
    
    private(set) lazy var cardView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(named: "cardColor")
        v.layer.cornerRadius = 4.0
        v.layer.shadowOpacity = 0.5
        v.layer.shadowColor = UIColor(white: 0.0, alpha: 0.2).cgColor
        v.layer.shadowRadius = 5.0
        v.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        return v
    }()
        
    private(set) lazy var eventImage: NetworkImageView = {
        let v = NetworkImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.clipsToBounds = true
        v.contentMode = .scaleAspectFill
        v.backgroundColor = .lightGray
        v.layer.cornerRadius = 25.0
        return v
    }()
    
    private(set) lazy var favoriteImage: UIImageView = {
        let v = UIImageView(image: UIImage(systemName: "heart.fill")?.withRenderingMode(.alwaysTemplate))
        v.tintColor = .red
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentMode = .scaleAspectFit
        return v
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor(named: "fontColor")
        v.numberOfLines = 2
        v.font = .systemFont(ofSize: 15.0, weight: .medium)
        return v
    }()
    
    private(set) lazy var locationLabel: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor(named: "fontColor")
        v.font = .systemFont(ofSize: 13.0, weight: .regular)
        return v
    }()
    
    private(set) lazy var dateLabel: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor(named: "fontColor")
        v.font = .systemFont(ofSize: 13.0, weight: .regular)
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        
        contentView.addSubview(cardView)
        
        cardView.addSubview(eventImage)
        cardView.addSubview(favoriteImage)
        cardView.addSubview(titleLabel)
        cardView.addSubview(locationLabel)
        cardView.addSubview(dateLabel)
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15.0),
            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15.0),
            cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15.0),
            cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15.0),
            
            eventImage.widthAnchor.constraint(equalToConstant: 50.0),
            eventImage.heightAnchor.constraint(equalToConstant: 50.0),
            eventImage.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 15.0),
            eventImage.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 15.0),
            
            favoriteImage.widthAnchor.constraint(equalToConstant: 20.0),
            favoriteImage.heightAnchor.constraint(equalToConstant: 20.0),
            favoriteImage.centerYAnchor.constraint(equalTo: eventImage.topAnchor),
            favoriteImage.centerXAnchor.constraint(equalTo: eventImage.leadingAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: eventImage.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: eventImage.trailingAnchor, constant: 10.0),
            titleLabel.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -15.0),
            
            locationLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            locationLabel.heightAnchor.constraint(equalToConstant: 15.0),
            locationLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5.0),
            locationLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            
            dateLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            dateLabel.heightAnchor.constraint(equalToConstant: 15.0),
            dateLabel.topAnchor.constraint(equalTo: locationLabel.bottomAnchor, constant: 5.0),
            dateLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)
        ])
    }
}


// MARK: - SKELETON
struct SearchEventSkeletonCellConfigurator: CellConfigurator {
    
    let cellIdentifier: String = SearchEventSkeletonCell.cellID()
    func configure(view: UIView) {}
}

final class SearchEventSkeletonCell: UICollectionViewCell {
    
    static var optimizedImageSize: Int = 50
    
    private(set) lazy var cardView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(named: "cardColor")
        v.layer.cornerRadius = 4.0
        v.layer.shadowOpacity = 0.5
        v.layer.shadowColor = UIColor(white: 0.0, alpha: 0.2).cgColor
        v.layer.shadowRadius = 5.0
        v.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        return v
    }()
        
    private(set) lazy var eventView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .lightGray
        v.layer.cornerRadius = 25.0
        v.alpha = 0.2
        return v
    }()
        
    private(set) lazy var titleView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .lightGray
        v.alpha = 0.2
        return v
    }()
    
    private(set) lazy var locationView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .lightGray
        v.alpha = 0.2
        return v
    }()
    
    private(set) lazy var dateView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .lightGray
        v.alpha = 0.2
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        
        contentView.addSubview(cardView)
        
        cardView.addSubview(eventView)
        cardView.addSubview(titleView)
        cardView.addSubview(locationView)
        cardView.addSubview(dateView)
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15.0),
            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15.0),
            cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15.0),
            cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15.0),
            
            eventView.widthAnchor.constraint(equalToConstant: 50.0),
            eventView.heightAnchor.constraint(equalToConstant: 50.0),
            eventView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 15.0),
            eventView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 15.0),
            
            titleView.topAnchor.constraint(equalTo: eventView.topAnchor),
            titleView.leadingAnchor.constraint(equalTo: eventView.trailingAnchor, constant: 10.0),
            titleView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -100.0),
            titleView.heightAnchor.constraint(equalToConstant: 15.0),
            
            locationView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            locationView.heightAnchor.constraint(equalToConstant: 15.0),
            locationView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 5.0),
            locationView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: -50.0),
            locationView.heightAnchor.constraint(equalToConstant: 15.0),
            
            dateView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            dateView.heightAnchor.constraint(equalToConstant: 15.0),
            dateView.topAnchor.constraint(equalTo: locationView.bottomAnchor, constant: 5.0),
            dateView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: -20.0),
            dateView.heightAnchor.constraint(equalToConstant: 15.0)
        ])
    }
}
