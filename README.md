# SeatGeek

Application interfacing with the SeatGeek API showcasing search, paging, favoriting and simple caching. No xibs, storyboards or third party libs.

## Installation

Clone repo and build to simulator or device - for iPhone or iPad

## License
[MIT](https://choosealicense.com/licenses/mit/)