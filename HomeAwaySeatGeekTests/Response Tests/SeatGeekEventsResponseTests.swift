//
//  SeatGeekEventsResponseTests.swift
//  HomeAwaySeatGeekTests
//
//  Created by swhittlesey on 11/4/20.
//

import XCTest
@testable import HomeAwaySeatGeek

class SeatGeekEventsResponseTests: XCTestCase {

    func testEventQueryResponseParsing() {
        
        guard let path = Bundle(for: SeatGeekEventsResponseTests.self).path(forResource: "seatGeekResponse", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail()
            return
        }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        do {
            let response = try decoder.decode(EventQueryResponse.self, from: data)
            
            XCTAssert(response.events.count == 10)
            
            // in production test other properties and structure etc..
            
        } catch {
           XCTFail("JSON Parsing For Event Query Failed")
        }
    }
    
    func testEventQueryToSystemModelConversion() {
        
        guard let path = Bundle(for: SeatGeekEventsResponseTests.self).path(forResource: "seatGeekResponse", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail()
            return
        }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        do {
            let response = try decoder.decode(EventQueryResponse.self, from: data)
            
            XCTAssert(response.events.count == 10)
            
            let events = response.systemModels
            
            XCTAssert(events.count == 10)
            
        } catch {
           XCTFail("JSON Conversion to System Models For Event Query Failed")
        }
    }
}
