//
//  ParsingStrategyTests.swift
//  HomeAwaySeatGeekTests
//
//  Created by swhittlesey on 11/5/20.
//

import XCTest
@testable import HomeAwaySeatGeek

class ParsingStrategyTests: XCTestCase {
    
    private struct SimpleJSONCodable: Codable {
        let aString: String
        let aInt: Int
    }
        
    func testJSONCodableParseSuccess() {
        
        let expectation = XCTestExpectation(description: "JSON Parse Success")
        
        let encoder = JSONEncoder()
        let simpleObject = SimpleJSONCodable(aString: "test", aInt: 10)
        
        let strategy = JSONCodableStrategy<SimpleJSONCodable>(decoder: JSONDecoder(), completion: { result in
            
            switch result {
            case .failure(let error):
                XCTFail("\(error)")
            case .success(let response):
                XCTAssert(response.aString == "test")
                XCTAssert(response.aInt == 10)
            }
            
            expectation.fulfill()
        })
        
        do {
            let data = try encoder.encode(simpleObject)
            strategy.handler(data, URLResponse(), nil)
        
        } catch {
            XCTFail("Failed to encode simple json object")
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testJSONCodableParseNoDataFail() {
        
        let expectation = XCTestExpectation(description: "JSON Parse Fail")
        
        let strategy = JSONCodableStrategy<EventQueryResponse>(completion: { result in
            
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success:
                break
            }
            
            expectation.fulfill()
        })
        
        strategy.handler(nil, URLResponse(), nil)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    // MARK: Image Parse
    func testImageDataParseSuccess() {
        
        guard let image = UIImage(named: "japan"),
              let imageData = image.pngData() else {
            XCTFail()
            return
        }
        
        let expectation = XCTestExpectation(description: "Image Parse Success")
        
        let strategy = ImageStrategy(imageMaxSize: 100, completion: { result in
            
            switch result {
            case .failure(let error):
                XCTFail("\(error)")
            case .success(let image):
                XCTAssertNotNil(image)
                XCTAssert(max(image.size.width * image.scale,
                              image.size.height * image.scale) == 100.0)
            }
            
            expectation.fulfill()
        })
        
        strategy.handler(imageData, URLResponse(), nil)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testImageDataParseDataFail() {
        
        let expectation = XCTestExpectation(description: "Image Parse Response Fail")
        
        let strategy = ImageStrategy(imageMaxSize: 100, completion: { result in
            
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success:
                XCTFail("should not succeed")
            }
            
            expectation.fulfill()
        })
        
        strategy.handler(nil, URLResponse(), nil)
        
        wait(for: [expectation], timeout: 1.0)
    }
}
