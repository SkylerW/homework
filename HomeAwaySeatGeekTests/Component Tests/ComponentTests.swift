//
//  ComponentTests.swift
//  HomeAwaySeatGeekTests
//
//  Created by swhittlesey on 11/7/20.
//

import XCTest
import Combine
import UIKit
@testable import HomeAwaySeatGeek

final class MockRouteDelegate: RouterDelegate {
    
    var shouldRoute: Bool = true
    var willRoute: Bool = false
    var didRoute: Bool = false
    
    init(shouldRoute: Bool) {
        self.shouldRoute = shouldRoute
    }
    
    func shouldRoute(viewController: UIViewController, fromRouter: Router) -> Bool {
        return shouldRoute
    }
    
    func willRoute(viewController: UIViewController, fromRouter: Router) {
        willRoute = true
    }
    
    func didRoute(viewController: UIViewController, fromRouter: Router) {
        didRoute = true
    }
}

class ComponentTests: XCTestCase {
    
    let viewController: UIViewController = UIViewController()
    let mockRouteDelegate = MockRouteDelegate(shouldRoute: true)
    
    var dataLoadingSubscriber: AnyCancellable?
    
    // NOTE: Just one short, compound example for sake of brevity
    func testEventResultsComponents() {
        
        let testEvent = Event(id: 1234,
                              title: "",
                              location: Location(city: "", state: ""),
                              date: Date(),
                              imageURL: nil,
                              keywords: [])
        
        let viewModel = EventResultsViewModel()
        XCTAssert(viewModel.dataLoading.value == false)
        XCTAssert(viewModel.collectionViewLayout.value is UICollectionViewCompositionalLayout)
        
        let router = EventResultsRouter()
        router.delegate = mockRouteDelegate
        
        router.showEventDetail(event: testEvent)
        
        XCTAssert(mockRouteDelegate.willRoute == true)
        XCTAssert(mockRouteDelegate.didRoute == true)
        
        let interactor = EventResultsInteractor(viewModel: viewModel, router: router)
        
        var loadingCheck = false
        
        dataLoadingSubscriber = viewModel.dataLoading.sink(receiveValue: { loading in
            XCTAssert(loading == loadingCheck)
        })
        
        loadingCheck = true
        interactor.displayLoadingState()
        
        XCTAssert(interactor.collectionViewManager.manageableCollections.count == 20)
        
        loadingCheck = false
        interactor.display(results: [testEvent], clearExisting: true)
        
        dataLoadingSubscriber = viewModel.dataLoading.sink(receiveValue: { loading in
            XCTAssert(loading == false)
        })
        
        XCTAssert(interactor.collectionViewManager.manageableCollections.count == 1)
        
        // NOTE: a full test suite will go further here and do more mock subscribers to the view model to make sure
        // reloads and collection view instructions are passed through as expected etc...
    }
}
