//
//  FileIOMocks.swift
//  HomeAwaySeatGeekTests
//
//  Created by swhittlesey on 11/5/20.
//

import Foundation
@testable import HomeAwaySeatGeek

final class FileIOTesting: FileIOLayer {
    
    lazy var storage: [URL: Data] = [:]
    
    var testURL: URL?
    
    init(url: URL? = nil) {
        testURL = url
    }
    
    func store(data: Data, to directory: URL, as fileName: String) {
        
        guard let url = testURL else {
            return
        }
        
        storage[url] = data
    }
    
    func retrieve(dataNamed fileName: String, from directory: URL) -> Data? {
        
        guard let url = testURL else {
            return nil
        }
        
        return storage[url]
    }
}
