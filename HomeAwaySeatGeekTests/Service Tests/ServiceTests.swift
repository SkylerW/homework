//
//  ServiceTests.swift
//  HomeAwaySeatGeekTests
//
//  Created by swhittlesey on 11/5/20.
//

import XCTest
@testable import HomeAwaySeatGeek

class ServiceTests: XCTestCase {

    lazy var session: URLSession = {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        return session
    }()
    
    lazy var networkLayer = NetworkLayerLive(session: session)
    
    lazy var searchService = SearchService(network: networkLayer,
                                           maxOperationCount: 1)
    
    lazy var imageService = ImageService(network: networkLayer,
                                         cache: ImageCache(),
                                         maxOperationCount: 10)
    
    lazy var favoritesService = FavoritesService(fileIOLayer: FileIOTesting(url: URL(string: "www.vrbo.com")!),
                                                 fileName: "test.json")
        
    let imageURL = URL(string: "https://www.vrbo.com/house.jpeg")!
    let jsonDataURL = URL(string: "https://api.seatgeek.com/2/events?client_id=\(SearchService.clientID)&q=coachella&page=1")!
    
    
    // MARK: SearchService
    func testSearchServiceSuccess() {
        
        guard let path = Bundle(for: ServiceTests.self).path(forResource: "seatGeekResponse", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail()
            return
        }
        
        URLProtocolMock.testURLs[jsonDataURL] = data
        
        let expectation = XCTestExpectation(description: "Search Service Success")
        
        try! searchService.search(withInput: SearchInput(input: "coachella")!, page: 1, response: { result in
            
            switch result {
            case .failure(let error):
                XCTFail("\(error)")
            case .success(let searchRes):
                XCTAssert(searchRes.events.count == 10)
            }
            
            expectation.fulfill()
        })
                        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testSearchServiceFailure() {
        
        let expectation = XCTestExpectation(description: "Search Service Fail")
        
        try! searchService.search(withInput: SearchInput(input: "mars")!, page: 1, response: { result in
            
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success:
                XCTFail("should not succeed")
            }
            
            expectation.fulfill()
        })
                        
        wait(for: [expectation], timeout: 1.0)
    }
    
    
    // MARK: ImageService
    func testImageServiceSuccess() {
        
        let image = UIImage(named: "japan")
        let imageData = image?.pngData()
        
        URLProtocolMock.testURLs[imageURL] = imageData!
        
        let expectation = XCTestExpectation(description: "Image Service Success")
        
        try! imageService.fetchImage(withURL: imageURL, imageMaxSize: 100, response: { result in
            
            switch result {
            case .failure(let error):
                XCTFail("\(error)")
            case .success(let imageTuple):
                XCTAssertNotNil(imageTuple.image)
                XCTAssertNotNil(imageTuple.url)
                XCTAssert(max(imageTuple.image.size.width * imageTuple.image.scale,
                              imageTuple.image.size.height * imageTuple.image.scale) == 100.0)
            }
                
            expectation.fulfill()
        })
                                
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testImageServiceFailure() {
        
        let image = UIImage(named: "japan")
        let imageData = image?.pngData()
        
        URLProtocolMock.testURLs[imageURL] = imageData!
        
        let expectation = XCTestExpectation(description: "Image Service Fail")
        
        try! imageService.fetchImage(withURL: URL(string: "www.fail.com")!, imageMaxSize: 100, response: { result in
            
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success:
                XCTFail("should not succeed")
            }
                
            expectation.fulfill()
        })
                        
        wait(for: [expectation], timeout: 1.0)
    }
    
    
    // MARK: FavoritesService
    func testFavoritesServiceActions() {

        let event = Event(id: 1234,
                          title: "test",
                          location: Location(city: "Austin",
                                             state: "TX"),
                          date: Date(),
                          imageURL: nil,
                          keywords: [])

        favoritesService.favorite(event: event, notifyChange: false)

        var favorites = favoritesService.favorites()

        XCTAssert(favorites.count == 1)
        XCTAssert(favoritesService.hasFavorite(event: event))

        favoritesService.unfavorite(event: event, notifyChange: false)

        favorites = favoritesService.favorites()

        XCTAssert(favorites.isEmpty)
    }
}
